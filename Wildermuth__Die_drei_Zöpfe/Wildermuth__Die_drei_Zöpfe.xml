<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title type="main">3. Die drei Zöpfe</title>
                <title type="sub"/>
                <author>
                    <persName ref="http://d-nb.info/gnd/118632833">
                        <forename>Ottilie</forename>
                        <surname>Wildermuth</surname>
                    </persName>
                </author>
                <respStmt>
                    <orgName/>
                    <resp>
                    </resp>
                </respStmt>
                <respStmt>
                    <orgName/>
                    <resp>
                    </resp>
                </respStmt>
            </titleStmt>
            <editionStmt>
                <edition/>
            </editionStmt>
            <extent>
                <measure type="tokens"/>
                <measure type="types"/>
                <measure type="characters"/>
            </extent>
            <publicationStmt>
                <publisher>
                    <email/>
                    <address>
                        <addrLine/>
                        <country/>
                    </address>
                </publisher>
                <pubPlace/>
                <date/>
                <availability xml:id="availability-textsource-1" corresp="#textsource-1">
                    <licence target="http://creativecommons.org/licenses/by/3.0/de/">
                        <p>CC-BY-3.0</p>
                    </licence>
                </availability>
                <idno/>
            </publicationStmt>
            <notesStmt>
                <note/>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title/>
                        <author/>
                    </titleStmt>
                    <extent/>
                    <publicationStmt>
                        <ab/>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <ab/>            
        </encodingDesc>
        <profileDesc>
            <creation>
                <date>
                    <date/>
                </date>
            </creation>
            <textClass>
                <keywords>
                    <list>
                        <item/>
                    </list>
                </keywords>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <listChange>
                <change/>
            </listChange>
        </revisionDesc>
    </teiHeader>
    <text>Die drei Zöpfe
Es gibt unterschiedliche Zöpfe. Das wurde auch die selige Ururgroßmutter mit Staunen gewahr, als ihre drei Buben, die man allesamt bei einem auswärtigen Präzeptor untergebracht hatte, der besonders berühmt in der Dressur war, in der ersten Vakanz nach Hause kamen. – Hatte sie doch alle drei vor der Abreise eigenhändig gewaschen und gestrählt, eigenhändig ihre widerspenstigen Haare mit Puder, Talg und Wachs behandelt, bis sie nach hinten gestrichen und dort zu einem steifen Zopf vereinigt waren, mit einem nagelneuen schwarzen Florettbande umwunden, so daß sie der Garde des Königs Friedrich Ehre gemacht hätten, wie sie abzogen, gleich gekleidet in glänzenden gestreiften Eternell, und die Zöpfe auf ihren Rücken tanzten.

Ja, es gibt unterschiedliche Zöpfe; wie verschieden sahen die Buben jetzt aus! Der älteste, der Heinrich, der ein hübscher Bursch war und allzeit gern den Herrn spielte, der hatte sich nimmer mit dem simplen Zopf nach väterlicher Weise begnügt, sondern er hatte extra ein paar Buckeln vorn, die gar zierlich ins Gesicht standen, und an dem Zopf noch eine besondere Schleife mit flatterndem Band als Zierat; so hatte er's bei dem Gefolge eines durchreisenden Fürsten gesehen, und so hatte er's mit der Hilfe von Präzeptors Heinrike zustande gebracht. Christian, der jüngste, machte mit seinem dünnen Schwänzlein keine solchen Ansprüche, das war nach wie vor säuberlich nach hinten gestrichen; nur das florettseidene Band hatte er an den Heinrich verhandelt und statt dessen ein altes Sackband mit Tinte schwarz gefärbt und damit den Zopf umwickelt. Der zweite aber, der dickbackige Gottlieb, der allzeit das Bequeme liebte, der hatte die Haare nur oben zusammengebunden und ließ sie nach unten frei, wie er solch fliegendes Haar schon bei Leichenbegleitungen gesehen; er meinte, so tue sich's auch. Was hatte die gute Mama für eine Not, bis sie die drei Zöpfe wieder zurecht gesalbt und in den normalen Zustand gebracht hatte! Auch gab man den Knaben am Schluß der Ferien ein Geleitschreiben an die Frau Präzeptorin mit, worin selbe höflich ersucht wurde, doch auch auf die Zöpfe der ihr anvertrauten Jugend zu achten und sie nimmer in so skandalösem Zustand nach Hause zu schicken.
Ob die Zöpfe von nun an in Ordnung geblieben sind, das weiß ich nicht; so viel aber weiß ich, daß jene Vakanzzöpfe bereits den künftigen Charakter der Buben vorbildlich darstellten. Heinrich, der war und blieb der Elegant; Christian kümmerte sich just nicht darum, ob sein Zöpfchen dick oder dünn, in Florettseide oder in Sackband gewickelt war, wenn er nur sonst sein Schäfchen ins Trockene brachte; Gottlieb aber, der wollte nichts als es gut haben auf der Welt, und weil's einmal ohne Zopf nicht ging, so wollte er sich den seinen wenigstens so bequem machen als möglich. Wenn der Herr Pate jedem der Buben einen Marktgroschen verehrte, so durfte 

man gewiß sein, daß sich Heinrich eine unechte Stecknadel und Gottlieb eine Wurst kaufte; Christian hingegen steckte den seinigen in einen Sparhafen, einen irdenen, von der sinnreichen Sorte, die nur eine Öffnung oben haben und die man zerschlagen muß, wenn man den Inhalt wieder haben will.
Mit stattlich gediehenen Zöpfen wurden die herangewachsenen Knaben zur bestimmten Zeit nacheinander auf die Universität spediert, um allda ihre Studien zu vollenden. Heinrich und Christian kamen ins berühmte theologische Stift, allwo jede honette Familie wenigstens einen Sprößling haben mußte; Gottlieb hatte eigentlich seine Laufbahn durch verschiedene Schreibstuben gemacht, sollte aber doch noch die gemeinen Rechte und etwas Humaniora studieren, um für ein städtisches Amt tauglich zu werden.
Die Zöpfe haben sie mitgenommen und redlich wieder heimgebracht. Keiner ist gewichen aus dem Gleis der angestammten Zucht und Sitte, wenn es auch da und dort kleine Abschweifungen gab. Heinrich war ein sehr strebsamer Geist; aber trotz seiner Begabung brachte er es zum Jammer von Papa und Mama niemals zum Primus. Er trieb allzeit Nebenstudien, Italienisch, Französisch, Heraldik und andre Allotria, und verletzte, wo er konnte, die heilige Stiftsordnung, um mit etlichen »Jungen von Adel« Jagdpartien und Fechtübungen mitzumachen; und nur seiner Rednergabe und, wie die böse Welt sagt, den auserlesenen Weinproben in Fäßchen, mit denen der Papa die Herren Professoren beehrte, hatte er's zu danken, daß er noch mit Ehren seine theologischen Studien absolvierte und nicht mit dem Kainszeichen eines hinausgeworfenen Stiftlers durch die Welt schweifen mußte.
Gottlieb, der band sich seinen Zopf bequem; er studierte gehörig, wenn auch nicht hitzig, aß und trank, so viel ihm schmeckte, ohne es in beiden Stücken zu übertreiben. So oft die regelmäßige Geldsendung vom Papa ankam (der alte Herr hielt streng auf feste Termine, obwohl er die Söhne nicht knapp hielt), schaffte sich Heinrich eine bordierte Weste, ein zierliches 

Petschaft oder irgend sonst einen Artikel an, mit dem er, trotz der strengen Kleiderordnung, die den Stiftlern Kutten vorschrieb, in seinen adligen Zirkeln Staat machen konnte, oder reichte es zur Miete für ein Reitpferd oder zu einem weltlichen Buch. Der Gottlieb aber, der lud seine Brüder zu einem Abendessen ein, wobei drei fette Enten verspeist und in edlem Uhlbacher des Papas Gesundheit getrunken wurde; er lachte dabei den Christian herzlich aus, der sich die Schlegel und Flügel der Ente fein säuberlich in ein Papier wickelte, um noch etliche Tage daran zu zehren. Sein Geld hatte Christian sorgsam verwahrt zu spärlichstem Gebrauch; er hatte seit seinen Knabenjahren bereits den dritten Sparhafen so weit gefüllt, daß nichts mehr hinunterfiel; unversehrt standen sie mit ihren dicken Bäuchen in einem geheimen Schiebfach seines Pults und lachten ihn mit ihren schiefen Mäulern an.
Aber den Zopf behielt er bei und auch das Florettband; seine äußere Erscheinung blieb jederzeit anständig, wenn sie gleich immer dünner und spitziger wurde.
Es gibt unterschiedliche Zöpfe. Das zeigt sich klärlich, wenn man die Bildnisse der drei Urgroßonkel aus ihren reifen Jahren betrachtet. Da ist zuvörderst der Heinrich, der hübsche Mann mit den feingeschnittenen Zügen, dessen Zopf als zierlicher Haarbeutel schalkhaft zwischen den schön geordneten Buckeln hervorblickt. Der feine dunkle Rock ist mit einem Orden geschmückt, und nur ein Paar zierliche Priesterkräglein bezeichnen noch den Theologen; mit spitzen Fingern nimmt er eben eine Prise aus der goldenen Tabatiere mit einem vornehmen Bildnis in seiner Hand und schaut lächelnd nach dem fürstlichen Lustschloß, das seitwärts durch ein Fenster zu sehen ist.
Heinrich war immer ein etwas leichtes Blut; der Westwind, der von jeher so viel Unsamen von Frankreich zu uns herüberwehte, hat ihn besonders stark angehaucht, und gar manchmal war er nahe daran gewesen, des respektabeln Namens seiner Väter unwert zu werden. Er hatte es verschmäht, als ehrbarer Vikarius unter den Fittichen des Papa sich zum gleichen Beruf 

vorzubereiten; seine hübsche Gestalt, seine feinen Manieren und die französische Sprachkenntnis hatten ihm eine glänzende Stelle als Hofmeister von zwei jungen Prinzen verschafft. Dieser Beruf führte ihn an fremde Höfe, in Bäder mit Spielbanken, in die Salons schöner Damen, ebensoviele Klippen für den welt- und lebenslustigen jungen Mann, und gar manche Kalypso hätte den Mentor beinahe eher als seine Telemachs festgehalten.
Aber der solide Geist des Vaterhauses, die Zucht der gestrengen Mutterhand, die ihm den ersten Zopf gebunden, hatten ihn nie ganz im Stiche gelassen, und er trug jetzt seinen Haarbeutel mit Ehren als wohlbestallter Oberkonsistorialrat. So war er nun der Stolz und der Glanz der Familie, der Herr Pate von allen Neffen, Nichten, Großnichten und Geschwisterkindeskindern; seinen Eltern wurde bei den damaligen Reiseschwierigkeiten nur einmal die Freude, ihn zu besuchen; aber dieser Besuch und die huldvolle Audienz bei den allerhöchsten Herrschaften blieben auch die Lichtpunkte ihrer Erinnerungen. Ich wollte euch nun gern die Einzelheiten aus dem Leben des Heinrich schildern, die zierlichen Unterhaltungen, die er mit der Frau Fürstin und dero hochfrisierten Hofdamen gepflogen; die geheimen, diplomatischen Sendungen, mit denen ihn der durchlauchtigste Herr beehrt hat; die französischen Lustspiele, die ihm die Hofsitte, unbeschadet seiner geistlichen Würde, zu dirigieren gestattet hatte: aber ich muß die Ausmalung solcher Szenen denen überlassen, die mehr daheim sind auf den Parkettböden der Hofsäle als ich.

Nun aber betrachtet Onkel Gottliebs Bild und sagt, ob euch dabei nicht das Herz lacht, wie er dasitzt, mit vergnüglichem Lächeln auf seinem wohlhäbigen breiten Gesicht, den schön geschliffenen Kelch mit funkelndem Wein in der Hand, während ein Aktenstoß zur Seite und das Rathaus im Hintergrund ihn als städtischen Beamten bezeichnen. Was hätte die Mama, die es nimmer erlebt hat, wie er Bürgermeister der guten Stadt H. wurde, was hätte sie für eine Freude, wenn sie sähe, wie er jetzt so guten Muts den dicken stattlichen Zopf, von dem er sich nun nicht mehr beengt fühlt, den Rücken hinunterhängen läßt! Er hatte es auch nicht nötig, sich etwas beengen zu lassen; es war ihm nach seines Herzens Wunsch ergangen, er konnte sich und andern das Leben leicht machen. Wie behaglich schaute einem schon von weitem seine Behausung auf dem Markt entgegen, mit dem blankpolierten Türschlosse, den spiegelhellen Scheiben, durch die man reichbefranste Vorhänge sah! Da war alles Fülle und Wohlbehagen, die gastliche Tafel des Herrn Bürgermeisters war weit berühmt in Stadt und Land. Er ließ sich niemals mit ausländischen Produkten ein: Austern, Kaviar und Champagner wurden in dem soliden schwäbischen Hause vermißt; aber alle guten Landeskinder: delikate Spargeln, feine Pflaumen und Aprikosen, Krebse, Forellen und Aale, auserlesene Trauben und reine Landweine von den edelsten Jahrgängen zierten die Tafel und erfreuten der Menschen Herz. Der Hühnerhof nährte Geflügel aller Art, Kapaunen, welsche und deutsche Hühner, Tauben und Gänse, die jederzeit bereit waren, ihr Leben im Dienste der Menschheit zu verhauchen. Es war kein Wunder, wenn angesichts dieser Herrlichkeit ein Bäuerlein meinte: wenn er der preußisch' König wär', er tät' sich nicht lang plagen mit dem Krieg, sondern zusehen, ob er nicht auch so ein »Deinstle« (Dienst) bekommen könnte. Daß aber Gottlieb daneben sein Amt tüchtig und eifrig verwaltete, Zeuge des ist der schöne Silberpokal, den ihm die dankbare Stadt für seine Verdienste verehrte und der als wertes Familienerbe noch bis heute aufbewahrt wird.

Auch der jüngste, der Christian, war zu Ehren und Würden gekommen; auf seinem Bildnis hat ihn der Maler mit großen Buchstaben als Seine Hochwürden den Herrn Spezial M*** bezeichnet. Besagtes Bildnis wurde ursprünglich auf Kosten des Stiftungsrats für die Sakristei im Akkord gemalt, den Mann à zwei Gulden, und ist daher kein künstlerisches Meisterstück, doch soll es ausgezeichnet getroffen sein.
Demnach ist der Onkel Spezial just keine 
beauté
 gewesen, erstaunlich lang und schmal; sein Gesicht hat die gelbliche Farbe und die spitzen Linien, wie man sie vorzugsweise bei Leuten findet, die für »ziemlich genau« (ein milder Ausdruck für geizig) gelten; sogar der Zopf entspricht dem übrigen: er ist auffallend lang, dünn und spitz.
Ich habe es noch nicht herausgebracht, ob die Leute reich werden, weil sie geizig sind, oder geizig werden, weil sie reich sind; es wäre eine interessante philosophische Examensaufgabe.
Bei dem Onkel Spezial schien beides in angenehmer Wechselwirkung zu stehen; reich war er unbestritten, und geizig ebenso gewiß, soweit sich solches mit Anstand und Schicklichkeit vertrug.
Das Zehentwesen hatte er bei seinem Vater daheim recht gründlich studiert, und somit war er, bei seiner natürlichen Begabung zum Sparen, vortrefflich befähigt zum Betriebe auch der materiellen Seite des Dekanatamtes. Übrigens ging er im Erwerben und Sparen niemals so weit, daß er seinem geistlichen Ansehen geschadet hätte, »der Zopf, der hing stets hinten«. Der üppige und liberale Haushalt des Bruders Gottlieb war ihm ein Greuel; er brauchte keinen solchen zu führen, war er doch sicher, wenigstens zwanzigmal des Jahres bei den jeweiligen Visitationen einen ausgesuchten Schmaus zu genießen, woran die Frau mit den lieben Kleinen auch Anteil nahm und wovon jedesmal eine vollgepackte Schachtel mit Viktualien, gar oft noch ein Schinken, ein Säckchen dürres Obst oder ein gefüllter Schmalzhafen bei der Heimfahrt in die Kutsche gepackt wurde. Da jede Frau Pfarrerin die beste Köchin sein wollte, so waren diese Schmausereien so reichlich, daß man 

gar lange aus der Erinnerung zehren und sich daheim mit Gemüs und Kartoffeln behelfen konnte.
Ein Spezial war damals noch eine ganz andre Respektsperson als heutzutage; den Pfarrern lag sehr viel daran, bei dem hochwürdigen Herrn in Gnaden zu stehen, damit ein günstiges Zeugnis dem Bericht ans Konsistorium beigelegt werde; darum hatten die Bötinnen vom Dorf fast allwöchentlich ein Küchengrüßlein für die Frau Spezialin im Korbe, also daß diese unter der Hand einen Kleinhandel mit Spargeln, Tafelobst und fettem Geflügel in die Residenz trieb, da solche Leckerbissen zu kostbar für die eigene Tafel erfunden wurden. Ein Wochenbett, das sagte der Onkel Christian im Vertrauen seinem Bruder Gottlieb, konnte er allezeit zu dreißig Gulden Reinertrag anschlagen.
Für Dienerschaft brauchte der Onkel Christian auch nicht viel auszugeben; der Mesner (Küster) war so eine Art von Haussklave im Spezialhause; er trug der Frau Spezialin im Winter 

den Fußsack in die Kirche und erwartete sie an der Pforte mit dem Schirm, wenn's regnete; er machte den Aufwärter bei den alljährlichen Disputationsessen, die der Dekan gegen anständige Vergütung zu halten genötigt war, wobei er zum Dank für seine Bemühung ein paar von den Rettichen, die er selbst als Beitrag zur Mahlzeit der Frau Spezialin verehrt hatte, »für seine Kinderlein« nach Hause mitbekam; er durfte in seinen Freistunden im Dekanatgarten arbeiten und sogar im verschlossenen Stalle Holz spalten. Seine Frau und Töchter leisteten Beistand bei Waschen, Putz- und Nähtereien, ohne daß ihnen einfiel, eine andre Belohnung zu erwarten als die drei Lebkuchen nebst einigen aufgefärbten Bändern, die sie zum Weihnachtsgeschenk erhielten.
Einige Gastlichkeit mußte freilich das Dekanathaus notgedrungen ausüben; es war ja bei Jahrmärkten und sonstigen wichtigen Veranlassungen die natürliche Heimat der Pfarrfrauen; auch wurde je und je ein Pfarrtöchterlein auf längeren Besuch geschickt, um der Frau Spezialin hilfreiche Hand zu leisten und zugleich Haushaltungskunst und feine Manieren von ihr zu lernen. Einmal erlaubte sich sogar ein junger unerfahrener Pfarrer, den Herrn Spezial wiederholt zu Gevatter zu bitten. Die Antwort, die er beim zweiten Versuch erhielt, ist so klassisch, daß ich nicht umhin kann, sie unverkürzt im Original mitzuteilen.
»Hochwohlehrwürdiger, Hochgelehrter, Insonders
Hochgeehrter Herr Gevatter!
Ich gestehe aufrichtig, daß ich einem abermaligen Ansinnen an mich und meine Frau zur Patenstelle von Ihrem neugeborenen Söhnlein nicht entgegengesehen habe. Meine Frau steht mit dem Löblichen Pfarrhaus in T. weder in einer Verbindung, noch hat sie eine gesucht, ich aber befinde mich mit Eurer usw. in einem amtlichen Verhältnis; aber ich bin in den Jahren schon so weit vorgerückt, daß ein Taufpate von meiner Seite für sein geistig- oder leibliches Wohl wenig oder nichts erwarten kann. Als Dieselben vor einem Jahr diesen Antrag an uns 

machten, so war es von Ihnen konsequent gehandelt, da Sie mir damit Anlaß gaben, mich durch das Patengeschenk der besonderen Verbindlichkeit zu entledigen, worin ich gegen Euer Hochehrw. usw. wegen zweimaligen Neujahrs- und den meinen beiden Töchtern gemachten Hochzeitsverehrungen stand. Nachdem Sie nun den vollen Ersatz dafür erhalten haben, so vermutete ich umso weniger, daß Ihre Absicht auf ein ferneres Geschenk von meiner Seite gehe, als Dieselben zum Hauptgrund der wiederholten Gevatterschaft unsre ununterbrochen fortgesetzte Gewogenheit angeben, und es sonderbar wäre, wenn diese noch von mir bezahlt werden sollte, hingegen entsage ich auch in Zukunft allen Geschenken von Ihnen.
Wahr ist es, daß durch die jährliche Visitation Ew. Hochehrw. meine Gegenwart Kosten verursacht, allein, nicht zu gedenken, daß mir daher doppelt angenehm war, wenn ich Dieselben mit der Frau Liebsten in meinem Hause wieder bewirten konnte, so werde ich auch mit dem besten Willen, wenn mein Amt mich ferner nach T. ruft, ihren lieben Kindern eine angemessene Verehrung machen, denn um meinetwillen geschieht doch der Aufwand nicht allein. Dies vorausgesetzt, so bezeuge ich unsre wahre Teilnahme an der abermaligen glücklichen Entbindung der Hochwerten Frau Liebsten, und bin ich auch mit meiner Frau zur Annahme der Patenstelle aus christlicher Gesinnung bereit, mit dem herzlichen Wunsch, daß Gott den Reichtum seiner Macht und Gnade an der Frau Wöchnerin wie an dem Säugling durch Leben und Wohltat in jeder Rücksicht verherrlichen und auch das Wachstum des lieben älteren Söhnleins begleiten möge.
Noch muß ich, teils aus Freundschaft, teils aus Amtspflicht die Bemerkung machen, daß mir und anderwärts die über den eigentlichen Bluts- oder Seitenverwandten noch sonstige Anzahl von Gevatterleuten aufgefallen; ich habe derselben niemals über fünf, meistens weniger gehabt, und ohne Taxe sind einem Privatmann von unsrer Klasse auch nie mehr erlaubt; dabei bedenken Sie noch den widrigen Eindruck, der einem Vorgesetzten und religiös denkenden Manne gegeben wird, 

wenn ein noch junger Geistlicher mit einer der heiligsten Handlungen eine kaufmännische Spekulation treibt!
Nach unsrer vielseitigen Empfehlung verharre ich noch mit schuldiger Hochschätzung
Euer
Hochehrwürden
gehorsamster Diener.«
Ob der Herr Pfarrer auf diesen Brief hin seinen frechen Antrag zurückgenommen, ist mir nicht bekannt geworden.
Ja, das mußte man dem Onkel Christian lassen, wenn er seinen Zopf auch mit gefärbtem Sackband umwand, so hatte er doch verstanden, sein Schäfchen trocken zu bringen, und wenn er seine Ersparnisse noch im irdenen Spartopf hätte aufbewahren wollen, er hätte einen bestellen müssen so groß wie ein Schulglobus.
Der Herbst ist der eigentliche Schwabenfrühling. Das lasse ich mir nicht nehmen, so absurd es klingen mag. Es steht dies gewiß im Zusammenhang mit der Sage, daß dem Schwaben der Verstand erst mit dem vierzigsten Jahre komme. Wir Schwaben haben zwar recht frühlingswarme Herzen und können gar schöne Lenzgedichte machen mit den verpönten Reimen: ziehen, blühen, Rose, Schoße, Mai, frei oder Treu; auch will ich dem Mai gewiß nichts Schlechtes nachreden: aber das muß ich doch im Vertrauen sagen, daß ich im Schwabenlande selten einen Frühling erlebt habe, in dem die Kirschenblüte nicht erfroren und die Apfelblüte nicht verregnet worden ist.
Frühlingslust, recht allgemeine volle Frühlingslust paßt für ein südlich Volk, dessen milder Boden ohne Mühe und Arbeit seine Früchte spendet. Was aber weiß unser Landmann von Maienwonne und Blütenlust, der eilen muß, seinen Dünger auf die Wiese zu bringen, und dessen Wintervorräte zu Ende sind! Auch die lieben Kinderlein, die man in Frühlingsbildern und Liedern im Ringeltanz auf dem Rasen abbildet, sind des 

Veilchenpflückens gar bald satt und seufzen nach der Zeit der materiellen Naturgenüsse: der Kirschen und Pflaumen und Birnen. Aber wie gesagt, ich bin weit entfernt, dem Frühling zu nahe zu treten, mit dem ich persönlich sehr intim stehe; nur müßt ihr mir zugeben, daß die rechte Freude erst da ist, wo jung und alt, reich und arm teilnehmen kann, wo von allen Hügeln Schüsse knallen und Schwärmer glänzen; wo der Segen vom Himmel auch die härtesten Herzen mildtätig gemacht hat und wo der ärmste Bettelknabe doch mit ein paar Äpfeln im Sack an einem Raine liegen und sich einen schönen Abend machen kann mit einigen halbausgebrannten Fröschen, die er von einem Herbstfeste erhascht hat.
»Es geht in Herbst« ist ein schwäbisches Trost-und Entschuldigungswort bei kleinen Mängeln, in die man sich mit Humor fügen muß. »Der sieht aus, als ob ihm der Herbst erfroren wäre« bezeichnet einen hohen Grad von Trübsal und Niedergeschlagenheit; kurz, der Herbst ist der rote Faden, der sich durchs Schwabenleben zieht; darum laßt's euch nicht zu viel werden, wenn in diesen Bildern der Herbst eine häufige Rolle spielt.
Macht mir nicht den Einwurf, daß in einem großen Teil von Schwaben keine oder saure Trauben wachsen. Herbst muß doch sein! Die mit den sauersten Trauben jubilieren am lautesten, und die, so gar keine haben, halten Kartoffelherbste und putzen acht Tage zuvor schon ihre alten Pistolen und neuen Büchsen, um sie recht laut krachen zu lassen.
Im Herbst allein sieht man keine neidischen und keine verhungerten Gesichter; im Herbst braucht keine Hand müßig zu sein, die sich rühren kann; das kleinste Mädchen schneidet ihr Kübelein mit Trauben, der kleinste Bube arbeitet mit den Füßen und lacht schelmisch hervor aus der Bütte, in der er auf und nieder tanzt. Im Herbst zieht der fröhliche Bursche in die Ferien heim und bringt ein frisches Leben in das verrostete Philistertum kleiner Städte.
Darum liegt für ein schwäbisches Herz ein süßer, geheimnisvoller Reiz in dem Duft des feuchten Herbstnebels, wenn er 

zum erstenmal wieder sich in die warme Sommerluft wagt, eine Erinnerung an fröhliche Herbstnächte, wo auflodernde Jugendlust und süße Wehmut wie Mondschein und Fackelglanz ihre magischen Lichter in junge Seelen werfen.
Auch die drei Brüder hatten sich von ihren Knabenjahren an des Herbstes gefreut. Sie hatten als Buben Versteckens in den Bütten gespielt und abwechselnd des Vaters Trauben zu Wein getreten, von denen sie zuvor im geheimen reichlichen Zehnten gezogen; sie hatten als Studenten um das lodernde Fackelfeuer das »
Gaudeamus igitur
« angestimmt, nicht einmal den Onkel Christian ausgenommen, der sogar an einem Herbstabend das Herz seiner nachmaligen »Frau Liebsten« erobert hatte. So dachten sie denn als Männer:
»So hab' ich's gehalten von Jugend an,
Und was ich als Ritter gepflegt und getan,
Nicht will ich's als Kaiser entbehren.«
Der Bürgermeister hatte sich den bestgelegenen Weinberg gekauft und ein schönes Lusthaus darin gebaut; da wurde denn der Herbst in Vergnügen und Herrlichkeit gefeiert, daß man weit und breit davon sprach, und von den vornehmsten »Regierungsherren«, die dazu gebeten wurden, bis zu dem niedrigsten Schützenbuben, der sich um die Behütung des Weinbergs Verdienste erworben, wußte jeder zu rühmen von dem fröhlichen Abend und dem freigebigen Herrn Bürgermeister. Auch Onkel Christian, zu dessen Stelle ein schöner Weinberg gehörte, tat ein übriges; es wurde ein Schinken abgesotten und Käse angerührt zu der Weinlese; ja, es durfte sich jeder seiner Knaben ein halb Dutzend Schwärmer dazu anschaffen!
Wie aber sollte der arme Heinrich den Herbst feiern in einer sandigen Residenz, wo kaum Kartoffeln wuchsen? Wohl gab es an der Hoftafel hie und da Trauben, in Gewächshäusern gezogen; aber was war das gegen eine Platte voll heimischer Silvaner, Rotwelsch und Muskateller? Die erlesenen Trauben, die ihm Bruder Gottlieb einmal in einer Schachtel ge schickt, waren auf den schlechten Wegen bei dem

 Mangel an ordentlicher Transportgelegenheit als ungenießbarer Most angekommen; sein Amt aber gestattete ihm nie, zur Herbstzeit eine Reise in die alte Heimat zu machen. – Da sprach er denn einmal seine Herbstsehnsucht recht wehmütig in einem Briefe an den Bruder Bürgermeister aus, und der wußte Rat zu schaffen. Es war in einem gesegneten Herbstjahr, als der Oberkonsistorialrat eben mit seiner Frau beim Kaffee saß und ihr vom schwäbischen Herbst erzählte; da kam ein plumper, schwerer Tritt die Treppe herauf, und eine derbe Stimme fragte: »Sind der Herr Konsestore daheim?« Noch ehe er nachsehen konnte, wer draußen sei, klopfte es mit der Faust an die Tür, und herein trat ein vierschrötiger Mann in der württembergischen Bauerntracht, mit einem sogenannten Reff auf dem Rücken, 

das voll bepackt war mit Schachteln, und an der Seite mit kunstreich verpfropften Krügen behängt. »Gotenobend, Herr Konsestore, en schöne Gruaß vom Herr Burgamoister, und da sollet Se au d'Trauba und da süaße Mohst versuacha!«
Nun war's eine Freude! Da lagen sie wohlgebettet und unversehrt im grünen Rebenlaub: Silvaner und Rotwelsche, Gutedel, Veltliner und Muskateller; daneben süßer Most in den Krügen, der just den kleinen »Stich« hatte, mit dem er am angenehmsten zu trinken ist, über Berg und Tal, über holperige Pfade und Flußfähren sicher getragen auf dem breiten Rücken des Matthes, des Leibweingärtners vom Bruder Gottlieb, der sich gegen reichliche Vergütung dazu verstanden hatte, seine Zöglinge selbst gut an Ort und Stelle zu bringen. Der Konsistorialrat, der feine Hofmann, vergoß helle Freudentränen über dies Stückchen brüderlicher Liebe; die Frau Fürstin selbst mußte mit höchsteigenem Munde die Erstlinge dieser süßen Schwabenkinder kosten, und bei einer fröhlichen Abendgesellschaft wurde in altem und neuem Wein die Gesundheit des freigebigen Bruders getrunken. Der Matthes, der der Meinung war, er sei fast bis ans Weltende gereist, so daß er nächstens über die Erdkugel hinuntergefallen wäre, wurde so herrlich verpflegt und reichlich beschenkt, daß er gern versprach, im nächsten Jahre wiederzukommen, als er nach drei Tagen abzog, seine Schachteln gefüllt mit den feinen Würsten, die das vornehmste Produkt der neuen, traubenarmen Heimat des Heinrich waren.
Seitdem zog Jahr für Jahr der ehrliche Sendbote durch drei deutsche Lande, um dem Heinrich den herbstlichen Brudergruß zu bringen, dem auch der Christian sein Scherflein beifügte, und es hat die Herzen der Brüder warm erhalten und das Gedächtnis an die Heimat jung und grün.
Wenn Ceres und Proserpina durch Samen und Blüten sich Grüße gesandt haben, warum sollten ein württembergischer Bürgermeister und ein fürstlicher Oberkonsistorialrat nicht durch Trauben und Würste in Rapport miteinander treten?
Das ist das netteste Stücklein, das ich euch zu erzählen weiß von den drei Urgroßonkeln mit den unterschiedlichen Zöpfen.</text>
</TEI>